package com.studios.applaudo.musicbox.fragment;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.studios.applaudo.musicbox.R;
import com.studios.applaudo.musicbox.service.MusicService;

/**
 * Created by Robert on 6/29/2016.
 */
public class MainFragment extends Fragment {

    MusicService musicService;
    boolean isBound = false;
    private boolean mCheckServiceRunning;
    ImageButton mPlayButton;
    private Intent mIntentPlay;
    private ImageView mImageView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.activity_main_content,container,false);
    }

    @Override
    public void onStart(){
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(MusicService.ACTION_PAUSE);
        intentFilter.addAction(MusicService.ACTION_PLAY); //calling intentfilters that we set in our broadcast
        intentFilter.addAction(MusicService.ACTION_STOP);
        getActivity().registerReceiver(mBroadcastfrg, intentFilter); //registering the broadcast and passing its intentfilters
        mIntentPlay = new Intent(getActivity(), MusicService.class);
        getActivity().bindService(mIntentPlay, serviceConnection, Context.BIND_AUTO_CREATE);
        isBound=true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        doUnBindService();
        getActivity().unregisterReceiver(mBroadcastfrg);
    }

    public void showMainContent(){
        mCheckServiceRunning = checkService(MusicService.class);
        final Intent broadPause = new Intent();
        final Intent broadPlay = new Intent();
        broadPause.setAction(MusicService.ACTION_PAUSE);
        broadPlay.setAction(MusicService.ACTION_PLAY);
        broadPlay.setAction(MusicService.ACTION_STOP);

        if (mIntentPlay == null){
            mIntentPlay = new Intent(getActivity(), MusicService.class);
        }

        if (getView() != null) {
            mImageView = (ImageView)getView().findViewById(R.id.mainImage_id);
            mPlayButton = (ImageButton) getView().findViewById(R.id.playBtn_id);
            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!mCheckServiceRunning){
                        getActivity().startService(mIntentPlay);
                        getActivity().bindService(mIntentPlay, serviceConnection, Context.BIND_AUTO_CREATE);
                        mPlayButton.setImageResource(R.mipmap.ic_pause);
                        mCheckServiceRunning = checkService(MusicService.class);
                        /* If our service is not running we wil start it, the  we will set the pause icon*/
                    }else{
                        pauseAction();
                    }
                }
            });
        }
    }

    void doUnBindService(){
        if (isBound){
            getActivity().unbindService(serviceConnection);
            isBound=false;
            /* We bound the activity to the service in order to have control of the play and pause buttons from the activity*/
        }
    }

    //connect to the service
    private ServiceConnection serviceConnection = new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MusicService.MyBinder binder = (MusicService.MyBinder) service;
            musicService = binder.getService();
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mCheckServiceRunning = checkService(MusicService.class);
            musicService=null;
        }
    };

    public BroadcastReceiver mBroadcastfrg = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String mAction = intent.getAction();

            switch (mAction){
                case MusicService.ACTION_PAUSE:
                    mPlayButton.setImageResource(R.mipmap.ic_play2);
                    musicService.pauseMusic();
                break;
                case MusicService.ACTION_PLAY:
                    musicService.playMusic();
                    mPlayButton.setImageResource(R.mipmap.ic_pause);
                break;
                case MusicService.ACTION_STOP:
                    getActivity().stopService(mIntentPlay);
                    mPlayButton.setImageResource(R.mipmap.ic_play2);
                    mCheckServiceRunning = false;
                    getActivity().unbindService(serviceConnection);
                    Log.d("IDENTIFIER: ",String.valueOf(mCheckServiceRunning));
                break;
                default:
                    getActivity().stopService(mIntentPlay);
                break;
            }
        }
    };

    public void pauseAction(){
        if(musicService!=null){
            if (musicService.isPlayingMusic()){
                musicService.pauseMusic(); //calling the method that is in our service
                mPlayButton.setImageResource(R.mipmap.ic_play2);
            }else {
                musicService.playMusic(); //calling the method that is in our service
                mPlayButton.setImageResource(R.mipmap.ic_pause);
            }
        }
    }

    //Checking if our service is on
    private boolean checkService(Class<?> MusicService){
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo: manager.getRunningServices(Integer.MAX_VALUE)){
            if (MusicService.getName().equals(serviceInfo.service.getClassName())){
                return true;
            }
        }
        return false;
    }
}
