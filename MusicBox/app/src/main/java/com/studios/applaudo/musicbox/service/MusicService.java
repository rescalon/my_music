package com.studios.applaudo.musicbox.service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;
import com.studios.applaudo.musicbox.MainActivity;
import com.studios.applaudo.musicbox.R;

/**
 * Created by Robert on 6/29/2016.
 */
public class MusicService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{

    private String urlToListen = "http://us1.internet-radio.com:8180/\n";
    private MediaPlayer mMediaPlayer;
    private boolean isRunning = false;
    public IBinder myBinder = new MyBinder();
    public final static String ACTION_PLAY = "actionPlay";
    public final static String ACTION_PAUSE = "actionPause";
    public final static String ACTION_STOP = "actionStop";

    public class MyBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        mMediaPlayer = new MediaPlayer();
        initialization();
    }

    public void initialization(){
        mMediaPlayer.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnPreparedListener(this);
        mMediaPlayer.setOnErrorListener(this);
    }

    @Override
    public int onStartCommand(Intent intent,int flags, int StartId){
        startRadio(); //once play button our service will start
        musicBar();
        Toast.makeText(MusicService.this, "SERVICE STARTED", Toast.LENGTH_SHORT).show();
        return Service.START_NOT_STICKY;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(mBroadcast);
        if(mMediaPlayer!=null){
            if(mMediaPlayer.isPlaying()){
                mMediaPlayer.stop();
                mMediaPlayer.release();
            }
        }
        stopSelf();
        stopForeground(true);
        Toast.makeText(MusicService.this, "SERVICE IS DONE", Toast.LENGTH_SHORT).show();
    }

    public void startRadio(){
        Uri radioUri = Uri.parse(urlToListen);
        try {
            mMediaPlayer.setDataSource(getApplicationContext(),radioUri); //getting the radio's url
        }catch (Exception e){
            e.printStackTrace();
        }
        mMediaPlayer.prepareAsync();
    }

    public void musicBar(){
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);
        /* When we tap on the notification, the main activity will be display*/

        IntentFilter intentFilter = new IntentFilter(ACTION_PLAY);
        intentFilter.addAction(ACTION_PAUSE);// we get this parameters from our broadcasat
        intentFilter.addAction(ACTION_STOP);

        registerReceiver(mBroadcast, intentFilter);
        PendingIntent pendingPause = PendingIntent.getBroadcast(this,0,new Intent(ACTION_PAUSE),0);
        PendingIntent pendingPlay = PendingIntent.getBroadcast(this,0,new Intent(ACTION_PLAY),0);
        PendingIntent pendingStop = PendingIntent.getBroadcast(this,0,new Intent(ACTION_STOP),0);

        /* We will set the notification's component like the icon and buttons*/
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.my_music_icon)
                .setContentTitle("MusicBox")
                .addAction(android.R.drawable.ic_media_pause, "PAUSE", pendingPause)
                .addAction(android.R.drawable.ic_media_play,"PLAY",pendingPlay)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel,"STOP", pendingStop);

        Notification notification =builder.build();
        startForeground(1, notification); //The notification will not be removed unless the service is killed
    }

    public void pauseMusic(){
        if (mMediaPlayer.isPlaying()){
            mMediaPlayer.pause();
        }
    }

    public void playMusic(){
        if(!mMediaPlayer.isPlaying()){
            mMediaPlayer.start();
        }
    }

    public boolean isPlayingMusic(){
        if(mMediaPlayer.isPlaying()){
            isRunning=true;
        }else {
            isRunning =false;
        }
        return isRunning;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        this.mMediaPlayer = mp;
        mMediaPlayer.start();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        return false;
    }

    public BroadcastReceiver mBroadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
             String mAction = intent.getAction();

            switch (mAction){
                case ACTION_PAUSE:
                    pauseMusic();
                break;
                case ACTION_PLAY:
                     playMusic();
                break;
                case ACTION_STOP:
                    mMediaPlayer.stop();
                    stopForeground(true);
                    stopSelf();
                break;
                default:
            }
        }
    };
}
