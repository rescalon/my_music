package com.studios.applaudo.musicbox;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.studios.applaudo.musicbox.fragment.MainFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_fragment);

        MainFragment mainFragment = (MainFragment)getSupportFragmentManager().findFragmentById(R.id.frgMain);
        mainFragment.showMainContent();

    }
}
